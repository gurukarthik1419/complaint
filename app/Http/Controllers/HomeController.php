<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
// ini_set('memory_limit', '-1');
DEFINE('BASE_URL','http://localhost/karthik/complaint');
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complaints=DB::table('complaints')->get();
        return view('complaint',['complaints'=>$complaints]);
    }

    
    public function complaint()
    {
    //    $complaints=DB::table('complaints')->get();

    $data = DB::table('complaints')->join('users','users.id','complaints.id')->get();
    foreach ($data as $key => $value) {
        $value->toComplaint = DB::table('users')->where('id',$value->officer)->first();
    }
    // print_r($data);exit;
        return view('complaint',['complaints'=>$data]);
    }

    public function postComplaint()
    {
        return view('post_complaint');
    } 
    public function single_complaint(Request $request)
    {
        
        $data = DB::table('complaints')->join('users','users.id','complaints.id')->where('complaints.id',$request->id)->first();
        if($data->officer == Auth::id()){
            DB::table('complaints')->where('id',$request->id)->update(['status'=>2]);
        }
        $data->toComplaint = DB::table('users')->where('id',$data->officer)->first();
        $data->comments = DB::table('complaint_comments')->join('users','users.id','complaint_comments.user_id')->where('complaint_id',$data->id)->get();
        // print_r($data);exit;
        return view('singleComplaint',['data'=>$data]);
    } 
      public function save_postComplaint(Request $request)
    {
        $check = DB::table('users')->where('id',Auth::id())->where('aadhar_card_number',$request->aadhar)->first();
        if(isset($check->id)){
            $requestData = $request->all();
            unset($requestData['_token']);
            $requestData['date'] = date('dd-mm-yyyy');
            $requestData['user_id'] = Auth::id();
            $update = DB::table('complaints')->insert($requestData);
            return back()->with('alert','Your Complaint is Posted!.');
        }else{
            return back()->with('error','you Enterd Aadhar number is incorrect');
        }
    }

      public function anncouncement(Request $request)
    {   
       $data=  DB::table('announcement')->join('users','users.id','announcement.user_id')->orderBy('announcement.id','desc')->get();
        // print_r($data);exit;
        return view('anncounce',['data'=>$data]);
    }

    public function updateUserInfo(Request $request){
        $requestData = $request->all();
        // print_r( $requestData);exit;
        // echo strlen($request->aadhar_card_number);exit;
        if(strlen($request->aadhar_card_number)<16 || strlen($request->aadhar_card_number)>16){
            return back()->with('error','Aadhar  number must be 16 charactor');
        }
        $then = strtotime($request->date_of_birth);
        //The age to be over, over +18
            $min = strtotime('+18 years', $then);
            // echo $min;
            if (time() < $min) {
                return back()->with('error','Your age Must be Gratherthen 18');
            }
        if(isset($requestData['kyc'])){
            $imageName = $request->file('kyc')->getClientOriginalName();
            $ext = $request->file('kyc')->getClientOriginalExtension();
            $imageName = rand(9090900,9090900).'.'.$ext;
            $request->file('kyc')->move(
                    'public/uploads/', $imageName
            );
          $requestData['kyc'] = BASE_URL."public/uploads/" . $imageName;
        }
        unset($requestData['_token']);
        $requestData['is_account_updated']=1;
        $update = DB::table('users')->where('id',Auth::id())->update($requestData);
        if($update){
        return redirect('/home')->with('alert','User Data Saved Sucussfully');
        }
    }


    public function addAnnouncement (Request $request){
        // print_r($request->all()); exit;
        $requestData = $request->all();
        $Add_date = $requestData['date'];
        $date = new Carbon;
        if($date > $Add_date){
            return back()->with('error','Please add Valid Date');
        }else{
        unset($requestData['_token']);
        DB::table('announcement')->insert($requestData);
        return redirect('/anncouncement')->with('alert','Anncouncement added Sucussfully');
        }
    }

    public function profile(){
       $data= DB::table('users')->where('id',Auth::id())->first();
       $anncounce=  DB::table('announcement')->where('user_id',Auth::id())->orderBy('id','desc')->get();
       $complaints=  DB::table('complaints')->where('user_id',Auth::id())->orderBy('id','desc')->get();
    //    print_r($complaints);exit;
        return view('profile',['data'=>$data,'anncounce'=>$anncounce,'complaints'=>$complaints]);
    }
    public function Editprofile(){
       $data= DB::table('users')->where('id',Auth::id())->first();
    //    $anncounce=  DB::table('announcement')->where('user_id',Auth::id())->orderBy('id','desc')->get();
        return view('editProfile',['data'=>$data]);
    }
    
    public function updateUser(Request $request){
        $requestData = $request->all();
        // print_r( $requestData);exit;
        $then = strtotime($request->date_of_birth);
    //The age to be over, over +18
        $min = strtotime('+18 years', $then);
        // echo $min;
        if (time() < $min) {
            return back()->with('error','Your age Must be Gratherthen 18');
        }

        if(isset($requestData['kyc'])){
            $imageName = $request->file('kyc')->getClientOriginalName();
            $ext = $request->file('kyc')->getClientOriginalExtension();
            $imageName = rand(9090900,9090900).'.'.$ext;
            $request->file('kyc')->move(
                    'public/uploads/', $imageName
            );
          $requestData['kyc'] = BASE_URL."/public/uploads/" . $imageName;
        }
        if(isset($requestData['profile_picture'])){
            $imageName = $request->file('profile_picture')->getClientOriginalName();
            $ext = $request->file('profile_picture')->getClientOriginalExtension();
            $imageName = rand(1111,9999).'.'.$ext;
            $request->file('profile_picture')->move(
                    'public/uploads/', $imageName
            );
          $requestData['profile_picture'] = BASE_URL."/public/uploads/" . $imageName;
        }
        unset($requestData['_token']);
        $update = DB::table('users')->where('id',Auth::id())->update($requestData);
        return back()->with('alert','User Data Saved Sucussfully');
    }

    public function getOffcier(Request $request){
        $data = [];
        if($request->state!='null'){
        $data= DB::table('users')->where('user_type',1)->where('state', 'like', '%' . $request->state . '%')->get();
        // echo $data->tosql();
        
        }
        if($request->taluk!='null'&& $request->state!='null'){
            $data= DB::table('users')->where('user_type',1)->where('state', 'like', '%' . $request->state . '%')->where('city', 'like', '%' . $request->taluk . '%')->get();
        }
        
        if($request->district!='null'&& $request->taluk!='null'&& $request->state!='null'){
            
            $data= DB::table('users')->where('user_type',1)->where('state', 'like', '%' . $request->state . '%')->where('city', 'like', '%' . $request->taluk . '%')
            ->where('district', 'like', '%' . $request->district . '%')->get();
        }
        
        // echo json_encode($data);
        return $data;
        
    }

    public function addComment(Request $request){
        $requestData = $request->all();
        unset($requestData['_token']);
        
        DB::table('complaint_comments')->insert($requestData);

        return back()->with('alert','Comments added sucussfully');
    }
    
}
