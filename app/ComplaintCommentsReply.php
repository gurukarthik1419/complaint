<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintCommentsReply extends Model
{
    //
    protected $table ="complaint_comments_replies";
}
