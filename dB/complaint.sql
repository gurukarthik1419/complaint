-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2019 at 05:58 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `complaint`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `location` text NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `user_id`, `title`, `location`, `date`, `description`, `created_at`, `updated_at`) VALUES
(1, 4, 'aa', 'aa', '2019-03-01', 'aa', '2019-03-17 06:37:20', '2019-03-17 06:37:20'),
(2, 4, 'aa', 'aa', '2019-03-01', 'aa', '2019-03-17 06:37:50', '2019-03-17 06:37:50'),
(3, 4, 'Tamil Nadu government announces special assistance of Rs 2,000 to BPL families', 'CHENNAI', '2019-03-08', 'The Tamil Nadu government unveiled a poverty alleviation scheme to dole out Rs 2,000 towards every family categorised Below Poverty Line in an allocation of Rs 1,200 crore for the forthcoming fiscal, Chief Minister Minister Edappadi K Palaniswami announced in the state Assembly today. The scheme is expected to reach out to 35 lakh families in the villages and 25 lakh urban poor families, Palaniswami read out in the Assembly. In his speech, Palaniswami said: “For all fa .', '2019-03-17 06:42:25', '2019-03-17 06:42:25'),
(4, 4, 'Tamil Nadu government announces special assistance of Rs 2,000 to BPL families', 'CHENNAI', '2019-03-29', 'aa', '2019-03-17 07:04:08', '2019-03-17 07:04:08'),
(5, 5, 'Tamil Nadu government announces special assistance of Rs 2,000 to BPL families', 'CHENNAI', '2019-03-28', 'aa', '2019-03-17 10:02:12', '2019-03-17 10:02:12');

-- --------------------------------------------------------

--
-- Table structure for table `complaint`
--

CREATE TABLE `complaint` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `officer` int(11) NOT NULL,
  `district` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `taluk` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aadhar` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`id`, `user_id`, `title`, `description`, `officer`, `district`, `taluk`, `state`, `category`, `status`, `date`, `aadhar`, `created_at`, `updated_at`) VALUES
(6, 6, 'Test', '11111111111111', 3, 'Trichy', 'Trichy south', 'Tamil Nadu', 'Water Problem', '1', '2424-0303-19191919', '11111111111111', '2019-03-24 16:41:12', '2019-03-24 16:41:12');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_comments`
--

CREATE TABLE `complaint_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `complaint_id` int(11) NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `complaint_comments_replies`
--

CREATE TABLE `complaint_comments_replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `complaint_comment_id` int(11) NOT NULL,
  `reply` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_13_093456_create_complaint', 1),
(4, '2019_01_13_093634_create_complaints_table', 1),
(5, '2019_01_13_094345_create_posts_table', 1),
(6, '2019_01_13_094409_create_post_comments_table', 1),
(7, '2019_01_13_094431_create_post_comments_replies_table', 1),
(8, '2019_01_13_094916_create_complaint_comments_table', 1),
(9, '2019_01_13_094930_create_complaint_comments_replies_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `venue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_comments`
--

CREATE TABLE `post_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_comments_replies`
--

CREATE TABLE `post_comments_replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_comment_id` int(11) NOT NULL,
  `reply` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aadhar_card_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_picture` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png',
  `mobile_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `user_type` int(11) NOT NULL DEFAULT '0',
  `kyc` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_account_updated` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `aadhar_card_number`, `profile_picture`, `mobile_number`, `state`, `city`, `district`, `date_of_birth`, `user_type`, `kyc`, `is_account_updated`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Karthikkumar', 'karthik@gmail.com', '$2y$10$zJq9BcqDa.O/C93GF3yO7e.f1BUJ9SG1jBAKZexW/sOG3ehAdRz0y', NULL, 'https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png', NULL, 'Tamil Nadu', 'Trichy south', 'Trichy', NULL, 1, NULL, 0, 'WCK6azBRDkdhkrxf5vEFkujYswTBY3rWiFDm99F0qOq4ieaepUQNc2W5A1MS', '2019-02-20 10:03:47', '2019-02-20 10:03:47'),
(2, 'kk', 'kk@kk.com', '$2y$10$C19i6Wt0mPauBaRkpF765eidX4PJminTObaWtaYIFDjNLcD819Iuq', NULL, 'https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png', NULL, 'Tamil Nadu', 'Trichy south', 'Trichy', NULL, 1, NULL, 0, 'evLzIbXW6jlcZiwmdptpTSHlvwzF97lDkvr44A7Qu2EjEfkp1Yr2eH2xNBhd', '2019-03-04 05:16:29', '2019-03-04 05:16:29'),
(3, 'Karthikkumar', 'karthik@kumar.com', '$2y$10$C7V2i3ZOzyEKc0l/0zVLlO.lCeMTEmkV423J3AVDOuz1ZuAOT2syC', NULL, 'https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png', NULL, 'Tamil Nadu', 'Trichy south', 'Trichy', NULL, 1, NULL, 0, NULL, '2019-03-09 23:40:19', '2019-03-09 23:40:19'),
(4, 'Karthik', 'karthik.bca1417@gmail.com', '$2y$10$jBD7ovNokXNEdtcknrqg7O8ziPU0ICOeJO/I7tSpX13PnafvIq0aK', NULL, 'https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png', NULL, 'Tamil Nadu', 'Trichy south', 'Trichy', NULL, 1, 'http://localhost/karthik/complaintpublic/uploads/9090900.png', 1, 'XkI7t3PZGTj9PEnOGLgXngiDZwikcRWPlR51hkVIOHuXTCvTkjls9ZKyEakE', '2019-03-17 00:27:25', '2019-03-17 00:27:25'),
(5, 'karthikkumarn', 'kumar@gmail.com', '$2y$10$olig1fIgB1lXkJHcX1EdnedIOOKv6/MeBhNPWdf7eEHGQij4crGYi', 'aaa', 'https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png', 'aa', 'Tamil Nadu', 'Trichy south', 'Trichy', '2019-03-08', 1, NULL, 1, NULL, '2019-03-17 03:49:28', '2019-03-17 03:49:28'),
(6, 'logeshkumar', 'logesh@logi.com', '$2y$10$OP3Ii0o24Cfc7cir/jiaXewVTmupqwhBpjqKMKDKGwMhvsjkwFuNu', '11111111111111', 'http://localhost/karthik/complaint/public/uploads/8768.jpg', '11111111111', 'Tamil Nadu', 'Trichy south', 'Trichy', '2019-03-28', 1, NULL, 1, NULL, '2019-03-24 00:12:54', '2019-03-24 00:12:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaint`
--
ALTER TABLE `complaint`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaint_comments`
--
ALTER TABLE `complaint_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaint_comments_replies`
--
ALTER TABLE `complaint_comments_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_comments`
--
ALTER TABLE `post_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_comments_replies`
--
ALTER TABLE `post_comments_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `complaint`
--
ALTER TABLE `complaint`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `complaint_comments`
--
ALTER TABLE `complaint_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complaint_comments_replies`
--
ALTER TABLE `complaint_comments_replies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_comments`
--
ALTER TABLE `post_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_comments_replies`
--
ALTER TABLE `post_comments_replies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
