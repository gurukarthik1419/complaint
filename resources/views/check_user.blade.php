@extends('layouts.app')

@section('content')
<title>User info</title>
<div class="account-tabs-setting">
<div class="row">

    <div class="col-lg-4" style="margin-top:20px;margin-left:536px">
        <div class="text-center white-text">
        <p class="heading-auth">Complaint Project</p>
        <br>
    </div>
	@if(Session::get('error'))
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>alert !</strong> {{Session::get('error')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
            

<div class="acc-setting justify-center align-center">

										<h3>Update User information</h3>
										<form method="POST" action="{{URL('/')}}/update-user-info" enctype="multipart/form-data">
                                        {{ csrf_field() }}
											<div class="cp-field">
												<h5>Select user type</h5>
												<div class="cpp-fiel">
												<ul class="on-off-status">
								<li>
									<div class="fgt-sec">
										<input type="radio" name="user_type" id="c5" class="user_type" value="0" checked>
										<label for="c5">
											<span></span>
										</label>
										<small>User</small>
									</div>
								</li>
								<li>
									<div class="fgt-sec">
										<input type="radio" name="user_type" id="c6" class="user_type" value="1">
										<label for="c6">
											<span></span>
										</label>
										<small>Goverment Officier</small>
									</div>
								</li>
							</ul>
												</div>
											</div>
											<div class="cp-field" id='aadhar'>
												<h5>Aadhar Card number</h5>
												<div class="cpp-fiel">
													<input type="number" minlength="16"  name="aadhar_card_number"  class="" placeholder="Aadhar Card number" id="aadhar_card_number" required>
													<i class="fa fa-american-sign-language-interpreting"></i>
												</div>
                                             
											</div>

                                            <div class="cp-field" id="kyc">
												<h5>Kyc Upload</h5>
												<div class="cpp-fiel">
													<input type="file" name="kyc" disabled id="kycField" placeholder="KYC" style="margin-top:3px" accept="image/*" required> 
													<i class="fa fa-upload"></i>
												</div>
                                             
											</div>

                                            <div class="cp-field" >
												<h5>Mobile Number</h5>
												<div class="cpp-fiel">
													<input type="text" name="mobile_number"  class="" placeholder="Mobile Number"  required>
													<i class="fa fa-phone"></i>
												</div>
                                             
											</div>
											<div class="cp-field" >
												<h5>State</h5>
												<div class="cpp-fiel">
													<input type="text" name="state"  class=""  placeholder="State"  required>
													<i class="fa fa-map-marker"></i>
												</div>
                                             
											</div>

                                            <div class="cp-field" >
												<h5>District</h5>
												<div class="cpp-fiel">
													<input type="text" name="district"  class="" placeholder="District"  required>
													<i class="fa fa-map-marker"></i>
												</div>
                                             
											</div>


                                            <div class="cp-field" >
												<h5>City/Street</h5>
												<div class="cpp-fiel">
													<input type="text" name="city"  class="" placeholder="City/Street"  required>
													<i class="fa fa-map-marker"></i>
												</div>
                                             
											</div>

                                            <div class="cp-field" >
												<h5>Date Of Birth</h5>
												<div class="cpp-fiel">
													<input type="date" name="date_of_birth"  class="" placeholder=""  required>
													<i class="fa fa-calendar"></i>
												</div>
                                             
											</div>
										
											<div class="save-stngs pd3">
												<ul>
                                                    <li><button type="submit" id="submit">Enter aadhar Continue</button></li>
												</ul>
											</div><!--save-stngs end-->
										</form>
									</div>
                                    </div>
									<br>
                                    <div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  $('#kyc').hide();
$('.user_type').change(function(){
   let type = $(this).val();
   if(type==0){
       $('#aadhar').show('slow');
       $('#kyc').hide('slow');
       $('#kycField').attr('disabled' ,'disabled');
       $('#aadhar_card_number').attr('disabled',false);
       $('#submit').html('Enter aadhar Continue');
   }else{
    $('#aadhar').hide('slow');
       $('#kyc').show('slow');
       $('#aadhar_card_number').attr('disabled','disabled');
       $('#kycField').attr('disabled',false);
       $('#submit').html('Upload and Continue');
   }
})
</script>
@endsection
