@extends('layout.master')
@section('title')
 Complaint - Water Problem in my city
@endsection
@section('content')

<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data">
                    <div class="posty">
											<div class="post-bar no-margin">
												<div class="post_topbar">
													<div class="usy-dt">
														<img src="{{$data->profile_picture}}" alt="" class="avatar" width="50px">
														<div class="usy-name">
															<h3>{{$data->name}}</h3>
															<span><img src="images/clock.png" alt=""><?php 								$now = new DateTime;
																							$full = false;
																							$ago = new DateTime($data->created_at);
																							$diff = $now->diff($ago);

																							$diff->w = floor($diff->d / 7);
																							$diff->d -= $diff->w * 7;

																							$string = array(
																								'y' => 'year',
																								'm' => 'month',
																								'w' => 'week',
																								'd' => 'day',
																								'h' => 'hour',
																								'i' => 'minute',
																								's' => 'second',
																							);
																							foreach ($string as $k => &$v) {
																								if ($diff->$k) {
																									$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
																								} else {
																									unset($string[$k]);
																								}
																							}

																							if (!$full) $string = array_slice($string, 0, 1);
																							echo $string ? implode(', ', $string) . ' ago' : 'just now';
																							
																							?></span>
														</div>
													</div>
												</div>
												
												<div class="job_descp">
													<h3>{{$data->title}}</h3>
													
													<p>{{$data->description}}</p>
													<ul class="job-dt">
														<li><a href="#" title="">{{$data->category}}</a></li>
													</ul>
												</div>
											</div><!--post-bar end-->
											<div class="comment-section">
												<div class="plus-ic">
													<h2 class="heading-auth" style="color: black!important">Comments</h2>
												</div>
												<div class="comment-sec">
													<ul>
													@if(count($data->comments)!=0)
													@foreach($data->comments as $comment)
														<li>
															<div class="comment-list">
																<div class="bg-img">
																	<img src="{{$comment->profile_picture}}" alt="" width="45px">
																</div>
																<div class="comment">
																	<h3>{{$comment->name}}</h3> 
																		<span><?php 								$now = new DateTime;
																							$full = false;
																							$ago = new DateTime($comment->created_at);
																							$diff = $now->diff($ago);

																							$diff->w = floor($diff->d / 7);
																							$diff->d -= $diff->w * 7;

																							$string = array(
																								'y' => 'year',
																								'm' => 'month',
																								'w' => 'week',
																								'd' => 'day',
																								'h' => 'hour',
																								'i' => 'minute',
																								's' => 'second',
																							);
																							foreach ($string as $k => &$v) {
																								if ($diff->$k) {
																									$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
																								} else {
																									unset($string[$k]);
																								}
																							}

																							if (!$full) $string = array_slice($string, 0, 1);
																							echo $string ? implode(', ', $string) . ' ago' : 'just now';
																							
																							?></span>
																	<p>{{$comment->comment}}</p>
																</div>
															</div><!--comment-list end-->
															<hr>
														</li>
														@endforeach
														@else
																							<li style="text-align:center">Be the first Comments</li>
																							<br>
																							<br>
																							<br>
																							<br>
																							<br>
														@endif

														
													</ul>
												</div><!--comment-sec end-->
												<!-- <form method="post" action="" -->
												<br>
												<br>
												<br>
												<div class="post-comment">
													<div class="comment_box ">
														<form action="{{URL('/')}}/comments" method="POST">
														{{ csrf_field() }}
															<input type="text" class="col-lg-12" placeholder="Post a comment" name="comment">
															<input type="hidden" class="col-lg-12" value="{{$data->id}}" name="complaint_id">
															<input type="hidden" class="col-lg-12" value="{{Auth::id()}}" name="user_id">
															<button type="submit">Send</button>
														</form>
													</div>
												</div><!--post-comment end-->
											</div><!--comment-section end-->
										</div>
				</div> 
			</div>
		</main>




	</div>
</main>
@endsection