@extends('layout.master')
@section('title')
 Complaint - Home 
@endsection
@section('content')
<?php 

$array  = [
	"Aarthi",
	"Ramya",
	"Geetha",
	"Keerthana",
	"Navamani",
	"Nithiya",
	"Hema",
	"Nanila",
	"Selvi",
	"Logesh",
	"Vinith",
	"Karthik",
];
// echo count($array);exit;
?>

<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data">
						<div class="row">

							@foreach ($complaints as $key => $value) 
							<div class="col-lg-4 col-md-8 no-pd">
								<br>
								<div class="main-ws-sec card-radius card">
									
									<div class="posts-section">
											<div class="post_topbar">
											@if($value->status==2)
											<span style="float:right"><span class="badge badge-success"> <i class="fa fa-check"></i> Viewd By Officier</span></span>
											@endif
												<div class="usy-dt cover avatar" >
													<img src="{{$value->profile_picture}}" alt="" class="avatar" width="50px">
													<div class="usy-name">
														<h3>{{$value->name}} </h3>
														<span><img src="images/clock.png" alt="">	<?php 								$now = new DateTime;
																							$full = false;
																							$ago = new DateTime($value->created_at);
																							$diff = $now->diff($ago);

																							$diff->w = floor($diff->d / 7);
																							$diff->d -= $diff->w * 7;

																							$string = array(
																								'y' => 'year',
																								'm' => 'month',
																								'w' => 'week',
																								'd' => 'day',
																								'h' => 'hour',
																								'i' => 'minute',
																								's' => 'second',
																							);
																							foreach ($string as $k => &$v) {
																								if ($diff->$k) {
																									$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
																								} else {
																									unset($string[$k]);
																								}
																							}

																							if (!$full) $string = array_slice($string, 0, 1);
																							echo $string ? implode(', ', $string) . ' ago' : 'just now';
																							
																							?></span>
													</div>
												</div>
											</div>
										
											<div class="job_descp">
												<h3>{{$value->title}}</h3>
												<hr>
												<p>{{$value->description}}</p>
												<ul class="job-dt">
													<li><a href="#" title="">{{$value->category}}</a></li>
												</ul>
												<ul class="">
													<li><a href="#" title="">complain @ {{$value->toComplaint->name}}</a></li>
												</ul>
											</div>
											<div class="job-status-bar">
												<ul class="like-com">
													 
													<li><a href="{{URL('/')}}/single_complaint?id={{$value->id}}" title="" class="com"><img src="images/com.png" alt="">Comments</a></li>
												</ul>
												<a href="{{URL('/')}}/single_complaint?id={{$value->id}}"><i class="la la-eye"></i>Views More</a>
											</div>
										</div><!--post-bar end-->
									
									</div><!--posts-section end-->
								</div><!--main-ws-sec end-->
								@endforeach


								</div>
								</div>
							</div>
						</div>
					</main>
					

	

	

	</div>
@endsection