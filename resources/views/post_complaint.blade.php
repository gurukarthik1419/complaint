@extends('layout.master')
@section('title')
 Complaint - Add Compliant 
@endsection
@section('content')
 <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
     


<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data col-md-10 col-md-offset-2">
                    <br><br>
										@if(Session::get('alert'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Wow !</strong> {{Session::get('alert')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@if(Session::get('error'))
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Wow !</strong> {{Session::get('error')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
                   
                    <div class="">
			<div class="post-project">
				<h3>Post a Complaint</h3>
				<div class="post-project-fields">
					<form method="post" action="{{URL('/')}}/post-complaint">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="row">
							<div class="col-lg-12">
								<input type="text" name="title" placeholder="Title" required>
							</div>
							<div class="col-lg-12">
								<div class="inp-field">
									<select name="category" required>
										<option disabled="" value="" selected="" >Select a Complaint category</option>
										<option value="Water Problem">Water Problem</option>
										<option value="Water Problem">Road problem</option>
										<option vlaue="Cleaning">Cleaning</option>
									</select>
								</div>
							</div>
							  <div class="col-lg-12">
								<div class="price-sec">
									<div class="price-br">
										<div class="inp-field">
									<select name="state" id="state">
										<option disabled="" value="" required selected="">Select a State</option>
										<option value="Tamil Nadu">Tamil Nadu</option>
									</select>
								</div>
									</div>
									<span> </span>
									<div class="price-br">
										<div class="inp-field">
										<select name="district" id="district">
										<option disabled="" value="" required selected="">Select a District</option>
										<option value="Trichy">Trichy</option>
									</select>
									</div>
									</div>

									<span> </span>
									<div class="price-br">
										<div class="inp-field">
										<select name="taluk" id="taluk">
										<option disabled="" value="" required selected="">Select a Taluk</option>
										<option value="Trichy south">Trichy south</option>
									</select>
									</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<select id="select" name="officer" required placeholder="Type Goverment Officier Name">
								
							</div>

                            <div class="col-lg-12">
								<input type="text" name="aadhar" required   placeholder="Type Your Aadhar Card Number">
							</div>

                          
							
							
							<div class="col-lg-12">
								<textarea name="description"  required placeholder="Description"></textarea>
							</div>
							<div class="col-lg-12">
								<ul>
									<li><button class="active" type="submit" value="post">Post</button></li>
									<li><a href="#" title="">Cancel</a></li>
								</ul>
							</div>
						</div>
					</form>
				</div><!--post-project-fields end-->
				<a href="#" title=""><i class="la la-times-circle-o"></i></a>
			</div><!--post-project end-->
		</div>
			</div>
		</main>




		
		
	
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	 <script>
  $('#district, #taluk, #state').change(function(){
		let state = $('#state').val();
		let taluk = $('#taluk').val();
		let district = $('#district').val();
		let query ="?state="+state+'&taluk='+taluk+'&district='+district;
		let select = "";
		$.ajax({
       url : "{{URL('/')}}/get-offcier"+query,
       method : "get",
         success: function(result){
						console.log(result);
					// let data = JSON.parse(result);
					for (let index = 0; index < result.length; index++) {
						const element = result[index];
						select +="<option value="+element.id+">"+element.name+"</option>";
					}
					if(select==""){
						select +="<option disabled selected>No Officiers found</option>";
					}
					$('#select').html(select);
					console.log(select);
        },
        error: function (result) {
            console.log('Error:', result);
        }
    });
	})
  </script>
@endsection