<header>
			<div class="container position-fixed">
				<div class="header-data">
					<div class="logo">
						<a href="{{URL('/')}}" title="" class="heading-auth">Complaint</a>
					</div><!--logo end-->
					<nav>
						<ul>

						<li>
								<a href="#" title="">
									<span><img src="{{URL('/')}}/images/icon4.png" alt=""></span>
									Complaint
								</a>
								<ul class="user-account-settingss">
									<li><a  style="color:black" href="{{URL('/')}}/post-complaint" title="">Post Complaint</a></li>
									<li><a style="color:black" href="{{URL('/')}}/complaint" title="">View Complaint</a></li>
								</ul>
							</li>


							
							
							<li>
								<a href="#"  title="">
								<span></span>
									<b>Announcement</b>
								</a>
								<ul class="user-account-settingss">
									<li><a  style="color:black" href="{{URL('/')}}/add-anncouncement" title="">Add Announcement</a></li>
									<li><a style="color:black" href="{{URL('/')}}/anncouncement" title="">View Announcement</a></li>
								</ul>
							</li>
							<li>
								
								
							</li>
							
							@if (Auth::guest())
                            <li>
						<span>	<a href="{{ route('login') }}">Login</a></span>
							</li>
                            <li>
							<span><a href="{{ route('register') }}">Register</a></span>
							</li>
                        @else
						</ul>
					</nav><!--nav end-->
					<div class="menu-btn">
						<a href="#" title=""><i class="fa fa-bars"></i></a>
					</div><!--menu-btn end-->
				
					<div class="user-account">
						<div class="user-info">
							<img src="{{Auth::user()->profile_picture}}" class="md-avatar rounded-circle" width="40px">
							<a href="#" title=""> {{ Auth::user()->name }} <i class="la la-sort-down"></i> </a>
							
						</div>
						<div class="user-account-settingss">
							
							<h3>Setting</h3>
							<ul class="us-links">
								<li><a href="{{URL('/')}}/profile" title="">Profile</a></li>
								<li><a href="#" title="">Privacy</a></li>
								<li><a href="#" title="">Faqs</a></li>
								<li><a href="#" title="">Terms & Conditions</a></li>
							</ul>
							<h3 class="tc"><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
													  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></h3>
						</div><!--user-account-settingss end-->
					</div>
					@endif
				</div><!--header-data end-->
			</div>
		</header><!--header end-->	


	
