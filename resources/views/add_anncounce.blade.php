@extends('layout.master')
@section('title')
 Complaint - Add Anncouncement 
@endsection
@section('content')
 <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
     


<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data col-md-10 col-md-offset-2">
                    <br><br>
                    
@if(Session::get('error'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Sorry !</strong> {{Session::get('error')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
                    <div class="">
			<div class="post-project">

			<h3>Add a new Accouncemnt</h3>
				<div class="post-project-fields">
					<form method="post" action="{{URL('/')}}/addAnnouncement">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="row">
							<div class="col-lg-12">
								<input type="hidden" name="user_id"  value="{{Auth::id()}}">
								<input type="text" name="title" placeholder="Title" value="" required>
							</div>
							
                            <div class="col-lg-12">
								<input type="text" name="location" placeholder="Location" required>
							</div>

                            <div class="col-lg-12">
								<input type="date" name="date" placeholder="date" required>
							</div>
							<div class="col-lg-12">
								<textarea name="description" placeholder="Description" required></textarea>
							</div>
							<div class="col-lg-12">
								<ul>
									<li><button class="active" type="submit" value="post">Add</button></li>
									<li><a href="{{URL('/')}}" title="">Cancel</a></li>
								</ul>
							</div>
						</div>
					</form>
				</div><!--post-project-fields end-->
				<a href="#" title=""><i class="la la-times-circle-o"></i></a>
			</div><!--post-project end-->
		</div>
			</div>
		</main>




		
		
	
	</div>
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	 <script>
  $( function() {
    var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];
    $( "#to_complaint" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
@endsection