@extends('layout.master')
@section('title')
 Complaint - {{$data->name}} 
@endsection
@section('content')
<?php 

$array  = [
	"Aarthi",
	"Ramya",
	"Geetha",
	"Nanila",
	"Logesh",
	"Vinith",
	"Karthik",
];
// echo count($array);exit;
?>
<section class="cover-sec">
			<img src="{{URL::asset('public/asset/images//resources/cover-img.jpg')}}" alt="">
		</section>

		<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data">
						<div class="row">
							<div class="col-lg-3" >
								<div class="main-left-sidebar">
									<div class="user_profile">
										<div class="user-pro-img">
											<img src="{{$data->profile_picture}}" alt=""  width="200px">
												<h3 style="font-weight: ">{{$data->name}}</h3>

												<div class="user_pro_status">
											<ul class="flw-hr">
												<br>
												<li><a href="{{URL('/')}}/edit-profile" title="" class="flww"><i class="la la-edit"></i> Edit Profile</a></li>
											</ul>
										</div>
									<div class="suggestions full-width">
										<div class="sd-title">
											<h3>Profile Info</h3>
										</div><!--sd-title end-->
										<div class="suggestions-list">
											<div class="suggestion-usd">
												<div class="sgt-text">
													<h4>Email</h4>
													<span>{{$data->email}}</span>
												</div>
												
											</div>
											<div class="suggestion-usd">
												<div class="sgt-text">
													<h4>Phone Number</h4>
													<span>{{$data->mobile_number}}</span>
												</div>
												
											</div>
											<div class="suggestion-usd">
												<div class="sgt-text">
													<h4>Aadhar Number</h4>
													<?php $aadhar = substr($data->aadhar_card_number, -4); ?>
													<span>XXXX XXXX XXXX {{$aadhar}}</span>
												</div>
												
											</div>
											<div class="suggestion-usd">
												<div class="sgt-text">
													<h4>State</h4>
													<span>{{$data->state}}</span>
												</div>
												
											</div>
											<div class="suggestion-usd">
												<div class="sgt-text">
													<h4>District</h4>
													<span>{{$data->district}}</span>
												</div>
												
											</div>
											<div class="suggestion-usd">
												<div class="sgt-text">
													<h4>City</h4>
													<span>{{$data->city}}</span>
												</div>
												
											</div>
											<div class="suggestion-usd">
												<div class="sgt-text">
													<h4>Date of birth</h4>
													<span>{{$data->date_of_birth}}</span>
												</div>
												
											</div>
											
										</div><!--suggestions-list end-->
									</div><!--suggestions end-->
								</div><!--main-left-sidebar end-->
							</div>
							</div>
							</div>
							<div class="col-lg-6">
								<div class="main-ws-sec">
									<div class="user-tab-sec">
										<h3>{{$data->name}}</h3>
										<div class="star-descp">
										<br>
										<span class="btn btn-primary white-text " style="color:white;cursor: pointer;" id="complaint">Complaints</span>
										<span class="btn btn-primary"  style="color:white;cursor: pointer;" id="announcement">Anncouncements</span>
										</div><!--star-descp end-->
										
									</div><!--user-tab-sec end-->
									<div class="product-feed-tab current" id="show_complaints" >
										<h2>Complaints</h2>
										<br>
											@foreach ($complaints as $key => $value) 
											<div class="posts-section">
									<div class=" post-barmain-ws-sec card-radius card">
									
									<div class="posts-section">
											<div class="post_topbar">
												<div class="usy-dt cover avatar" >
													<img src="{{$data->profile_picture}}" alt="" class="avatar" width="40px">
													<div class="usy-name">
														<h3>{{$data->name}}</h3>
														<span><img src="images/clock.png" alt="">3 min ago</span>
													</div>
												</div>
											</div>
											<div class="job_descp">
												<h3>{{$value->title}}</h3>
												<hr>
												<ul class="job-dt">
													<li><a href="#" title="">{{$value->category}}</a></li>
												</ul>
												<ul class="">
												</ul>
											</div>
											<div class="job-status-bar">
												<ul class="like-com">
													 
													<li><a href="{{URL('/')}}/single_complaint/{{$value->id}}" title="" class="com"><img src="images/com.png" alt=""> Comments</a></li>
												</ul>
												<a href="{{URL('/')}}/single_complaint/{{$value->id}}"><i class="la la-eye"></i>Views More</a>
											</div>
										</div><!--post-bar end-->
									
									</div><!--posts-section end-->
									<br>
								</div><!--main-ws-sec end-->	
								<br>
								@endforeach
										
			
				</div>


				<div class="product-feed-tab current" id="show_announcements" >
								<h2>Announcements</h2>
										<br>
											@foreach ($anncounce as $key => $value) 
											
											<div class="posts-section">
									<div class=" post-barmain-ws-sec card-radius card">
									
									<div class="posts-section">
											<div class="post_topbar">
												<div class="usy-dt cover avatar" >
													<img src="{{$data->profile_picture}}" alt="" class="avatar" width="40px">
													<div class="usy-name">
														<h3>{{$data->name}}</h3>
														<span><img src="images/clock.png" alt="">{{$data->created_at}}</span>
													</div>
												</div>
											</div>
											<div class="job_descp">
												<h3>{{$value->title}}</h3>
											</div>
											
										</div><!--post-bar end-->
									
									</div><!--posts-section end-->
									<br>
								</div><!--main-ws-sec end-->	
								<br>
								@endforeach
										
			
				</div>
				</div>
				</div>
				</div>
				</div>
				</div>
				</div>
				</main>
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$('#complaint').css('background-color','#095d32');
$('#complaint').click(function(){
	$('#show_announcements').hide('slow');
	$('#show_complaints').show('slow');
	$(this).css('background-color','#095d32');
	$('#announcement').css('background-color','#007bff');
});


$('#announcement').click(function(){
	$('#show_announcements').show('slow');
	$('#show_complaints').hide('slow');
	$(this).css('background-color','#095d32');
	$('#complaint').css('background-color','#007bff');
});
</script>
@endsection