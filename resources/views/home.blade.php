@extends('layout.master')
@section('title')
 Complaint - Home 
@endsection
@section('content')
<?php 

$array  = [
	"Aarthi",
	"Ramya",
	"Geetha",
	"Keerthana",
	"Navamani",
	"Nithiya",
	"Hema",
	"Nanila",
	"Selvi",
	"Logesh",
	"Vinith",
	"Karthik",
];
// echo count($array);exit;
?>

<main>
			<div class="main-section">
				<div class="container">
					<div class="main-section-data">
						<div class="row">

							@foreach ($array as $key => $value) 
							<div class="col-lg-4 col-md-8 no-pd">
								<br>
								<div class="main-ws-sec card-radius card">
									
									<div class="posts-section">
											<div class="post_topbar">
												<div class="usy-dt cover avatar" >
													<img src="https://pngimage.net/wp-content/uploads/2018/05/dummy-profile-image-png-2.png" alt="" class="avatar" width="40px">
													<div class="usy-name">
														<h3>{{$value}}</h3>
														<span><img src="images/clock.png" alt="">3 min ago</span>
													</div>
												</div>
											</div>
										
											<div class="job_descp">
												<h3>Water problem in my city</h3>
												<hr>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam luctus hendrerit metus, ut ullamcorper quam finibus at.</p>
												<ul class="job-dt">
													<li><a href="#" title="">Water Problem</a></li>
												</ul>
												<ul class="">
													<li><a href="#" title="">complain @Aarthi</a></li>
												</ul>
											</div>
											<div class="job-status-bar">
												<ul class="like-com">
													 
													<li><a href="{{URL('/')}}/single_complaint" title="" class="com"><img src="images/com.png" alt=""> Comment 15</a></li>
												</ul>
												<a href="{{URL('/')}}/single_complaint"><i class="la la-eye"></i>Views More</a>
											</div>
										</div><!--post-bar end-->
									
									</div><!--posts-section end-->
								</div><!--main-ws-sec end-->
								@endforeach


								</div>
								</div>
							</div>
						</div>
					</main>
					

	

	

	</div>
@endsection