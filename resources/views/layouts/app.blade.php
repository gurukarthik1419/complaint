
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Complaint Project</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/css/animate.css')}}">
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/css/line-awesome.css')}}">
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/css/line-awesome-font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/css/jquery.mCustomScrollbar.min.css')}}">
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/lib/slick/slick.css')}}">
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/lib/slick/slick-theme.css')}}">
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/css/style.css')}}">
<link rel="stylesheet" type="text/css"href="{{URL::asset('public/asset/css/responsive.css')}}">

<style>
.help-block{
    font-weight:bold;
    color:red;
    padding:5px;
    margin-top:10px;

}
.has-error{
    border :1px solid red!important;

}
.heading-auth{
	font-size:30px;
	color:#fff;
	font-weight: bold;
}
</style>

</head>
<body  style="background: url('https://i.pinimg.com/originals/ff/d7/a5/ffd7a533c81a9f167c5ee3cc0bd5d511.jpg')">
<div class="wrapper justify-center"  >
<br>
<br>
<br>
<br>
<br>

@yield('content')
</div>
</body>

<script type="text/javascript" src="{{URL::asset('public/asset/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/asset/js/popper.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/asset/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/asset/js/jquery.mCustomScrollbar.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/asset/lib/slick/slick.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/asset/js/scrollbar.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/asset/js/script.js')}}"></script>

</body>
<script>'undefined'=== typeof _trfq || (window._trfq = []);'undefined'=== typeof _trfd && (window._trfd=[]),_trfd.push({'tccl.baseHost':'secureserver.net'}),_trfd.push({'ap':'cpsh'},{'server':'a2plcpnl0235'}) // Monitoring performance to make your website faster. If you want to opt-out, please contact web hosting support.</script><script src='../../../img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js')}}'></script>
<!-- Mirrored from gambolthemes.net/workwise_demo/HTML/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Jan 2019 12:23:09 GMT -->
</html>