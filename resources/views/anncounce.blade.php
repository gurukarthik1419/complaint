@extends('layout.master')
@section('title')
 Complaint - Anncouncement 
@endsection
@section('content')
<?php 

$array  = [
	"Aarthi",
	"Ramya",
	"Geetha",
	"Keerthana",
	"Navamani",
	"Nithiya",
	"Hema",
	"Nanila",
	"Selvi",
	"Logesh",
	"Vinith",
	"Karthik",
];
// echo count($array);exit;
?>

<main>
			<div class="main-section">
				<div class="container">
				@if(Session::get('alert'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Wow !</strong> {{Session::get('alert')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
					<div class="main-section-data">
						<div class="row">
		@if(count($data)!=0)
							@foreach ($data as $key => $value) 
							<div class="col-lg-6 col-md-8 no-pd">
								<br>
								<div class="main-ws-sec card-radius card">
									
									<div class="posts-section">
											<div class="post_topbar">
												<div class="usy-dt cover avatar" >
													<img src="{{$value->profile_picture}}" alt="" class="avatar" width="40px">
													<div class="usy-name">
														<h3>{{$value->name}}</h3>
														<span><img src="images/clock.png" alt="">
														<?php 								$now = new DateTime;
																							$full = false;
																							$ago = new DateTime($value->created_at);
																							$diff = $now->diff($ago);

																							$diff->w = floor($diff->d / 7);
																							$diff->d -= $diff->w * 7;

																							$string = array(
																								'y' => 'year',
																								'm' => 'month',
																								'w' => 'week',
																								'd' => 'day',
																								'h' => 'hour',
																								'i' => 'minute',
																								's' => 'second',
																							);
																							foreach ($string as $k => &$v) {
																								if ($diff->$k) {
																									$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
																								} else {
																									unset($string[$k]);
																								}
																							}

																							if (!$full) $string = array_slice($string, 0, 1);
																							echo $string ? implode(', ', $string) . ' ago' : 'just now';
																							
																							?></span>
													</div>
												</div>
											</div>
										
											<div class="job_descp" style="line-height: 35px">
												<h3>{{$value->title}}</h3>
												<hr>
												<p>{{$value->description}} 
</p>
												<ul class="">
													<li><a href="#" title="">Announce by @ {{$value->name}}</a></li>
												</ul>
											</div>
										</div><!--post-bar end-->
									
									</div><!--posts-section end-->
								</div><!--main-ws-sec end-->
								@endforeach
								@else 
								<div class="" style="    text-align: center;
    margin: auto;">
				<img src="http://cgcollege.org/Assets/images/icons/nodata-found.png" >
				</div>
								@endif


								</div>
								</div>
							</div>
						</div>
					</main>
					

	

	

	</div>
@endsection