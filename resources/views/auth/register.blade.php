@extends('layouts.app')

@section('content')
<div class="account-tabs-setting">
<div class="row">

    <div class="col-lg-4" style="margin-left:536px">
    	<div class="text-center white-text">
        <p class="heading-auth">Complaint Project</p>
        <br>
    </div>
<div class="acc-setting">

										<h3>Register</h3>
										<form method="POST" action="{{ route('register') }}">
                                        {{ csrf_field() }}

											<div class="cp-field">
												<h5>Name</h5>
												<div class="cpp-fiel">
													<input type="text" name="name" class="{{$errors->has('name') ? 'has-error' : '' }}" placeholder="Name">
													<i class="fa fa-envelope"></i>
                                                    <br>
                                                    @if ($errors->has('name'))
                                                        <div class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </div>
                                                    @endif
												</div>
											</div>

                                            <div class="cp-field">
												<h5>Email</h5>
												<div class="cpp-fiel">
													<input type="text" name="email" class="{{$errors->has('email') ? 'has-error' : '' }}" placeholder="Email">
													<i class="fa fa-envelope"></i>
                                                    <br>
                                                    @if ($errors->has('email'))
                                                        <div class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </div>
                                                    @endif
												</div>
											</div>
											<div class="cp-field">
												<h5>Password</h5>
												<div class="cpp-fiel">
													<input type="password" name="password"  class="{{$errors->has('password') ? 'has-error' : '' }}" placeholder="Password">
													<i class="fa fa-lock"></i>
												</div>
                                                @if ($errors->has('password'))
                                    <div class="help-block">
                                    <br>
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
											</div>

                                            <div class="cp-field">
												<h5>Password</h5>
												<div class="cpp-fiel">
													<input type="password" name="password_confirmation"  class="{{$errors->has('password_confirmation') ? 'has-error' : '' }}" placeholder="Confirm Password">
													<i class="fa fa-lock"></i>
												</div>
                                                @if ($errors->has('password'))
                                    <div class="help-block">
                                    <br>
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
											</div>
										
											<div class="save-stngs pd3">
												<ul>
													<li><button type="submit">Register</button></li>
												<li> Already have account ? &nbsp;<a href="{{URL('/')}}/login" style="font-weight: bold">Login</a></li>
												</ul>
											</div><!--save-stngs end-->
										</form>
									</div>
                                    </div>
                                    <div>
                                    
    </div>
@endsection
