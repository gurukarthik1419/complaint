@extends('layouts.app')

@section('content')
<div class="account-tabs-setting">
<div class="row">

    <div class="col-lg-4" style="margin-top:40px;margin-left:536px">
        <div class="text-center white-text">
        <p class="heading-auth">Complaint Project</p>
        <br>
    </div>
<div class="acc-setting justify-center align-center">

										<h3>Login</h3>
										<form method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}

											<div class="cp-field">
												<h5>Email</h5>
												<div class="cpp-fiel">
													<input type="text" name="email" class="{{$errors->has('email') ? 'has-error' : '' }}" placeholder="Email">
													<i class="fa fa-envelope"></i>
                                                    <br>
                                                    @if ($errors->has('email'))
                                                        <div class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </div>
                                                    @endif
												</div>
											</div>
											<div class="cp-field">
												<h5>Password</h5>
												<div class="cpp-fiel">
													<input type="password" name="password"  class="{{$errors->has('password') ? 'has-error' : '' }}" placeholder="Password">
													<i class="fa fa-lock"></i>
												</div>
                                                @if ($errors->has('password'))
                                    <div class="help-block">
                                    <br>
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
											</div>
										
											<div class="save-stngs pd3">
												<ul>
                                                    <li><button type="submit">login</button></li>
													<li>Don't have account ? &nbsp;<a href="{{URL('/')}}/register" style="font-weight: bold">Register</a></li>
												</ul>
											</div><!--save-stngs end-->
										</form>
									</div>
                                    </div>
                                    <div>

<!--                                     
<div class="container">


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
