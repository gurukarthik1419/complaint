@extends('layout.master')

@section('title')
 Complaint - Edit Profile 
@endsection
@section('content')
<title>User info</title>
<div class="account-tabs-setting">
<div class="row">

    <div class="col-lg-5" style="margin-top:20px;margin-left:416px">
        <div class="text-center white-text">
        <p class="heading-auth" style="color:black">Complaint Project</p> <br>
        <br>
    </div>
	@if(Session::get('alert'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Wow !</strong> {{Session::get('alert')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if(Session::get('error'))
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Error !</strong> {{Session::get('error')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="acc-setting justify-center align-center">

										<h3>Update User information</h3>

										

										<form method="POST" action="{{URL('/')}}/update-user" enctype="multipart/form-data">
                                        {{ csrf_field() }}
										<div class="cp-field">
										<div class="usr-pic">
													<img src="{{$data->profile_picture}}" alt="">
												</div>
												<input type="file" name="profile_picture" style="margin-top:90px" style="text-align:center">
												</div>
											
											<div class="cp-field">
											<hr>
												<h5>Select user type</h5>
												<div class="cpp-fiel">
												<ul class="on-off-status">
							@if($data->user_type==0)
								<li>
									<div class="fgt-sec">
										<input type="radio" name="user_type" id="c5" class="user_type" value="0" checked>
										<label for="c5">
											<span></span>
										</label>
										<small>User</small>
									</div>
								</li>
								@else
								<li>
									<div class="fgt-sec">
										<input type="radio" name="user_type" id="c6" class="user_type" value="1" checked>
										<label for="c6">
											<span></span>
										</label>
										<small>Goverment Officier</small>
									</div>
								</li>
								@endif
							</ul>
												</div>
											</div>
											@if($data->user_type==0)
											<div class="cp-field" >
												<h5>Aadhar Card number</h5>
												<div class="cpp-fiel">
												<?php $aadhar = substr($data->aadhar_card_number, -4); ?>
													<input type="text" name="aadhar_card_number"  disabled class="" value="XXXX XXXX {{$aadhar}}" placeholder="Aadhar Card number" id="aadhar_card_number" required>
													<i class="fa fa-american-sign-language-interpreting"></i>
												</div>
                                             
											</div>
					@else
                                            <div class="cp-field" >
												<h5>Kyc Upload</h5>
												<div class="cpp-fiel">
													<input type="file" name="kyc"  id="kycField" placeholder="KYC" style="margin-top:3px" accept="image/*" required> 
													<i class="fa fa-upload"></i>
												</div>
                                             
											</div>
											@endif

											<div class="cp-field" >
												<h5>Email</h5>
												<div class="cpp-fiel">
													<input type="text" name="email" value="{{$data->email}}"  class="" placeholder="Email"  required>
													<i class="fa fa-envelope"></i>
												</div>
                                             
											</div>

											<div class="cp-field" >
												<h5>Name</h5>
												<div class="cpp-fiel">
													<input type="text" name="name" value="{{$data->name}}"  class="" placeholder="Name"  required>
													<i class="fa fa-user"></i>
												</div>
                                             
											</div>

                                            <div class="cp-field" >
												<h5>Mobile Number</h5>
												<div class="cpp-fiel">
													<input type="text" name="mobile_number" value="{{$data->mobile_number}}"  class="" placeholder="Mobile Number"  required>
													<i class="fa fa-phone"></i>
												</div>
                                             
											</div>
											<div class="cp-field" >
												<h5>State</h5>
												<div class="cpp-fiel">
													<input type="text" name="state"  class="" value="{{$data->state}}" placeholder="State"  required>
													<i class="fa fa-map-marker"></i>
												</div>
                                             
											</div>

                                            <div class="cp-field" >
												<h5>District</h5>
												<div class="cpp-fiel">
													<input type="text" name="district"  class="" value="{{$data->district}}" placeholder="District"  required>
													<i class="fa fa-map-marker"></i>
												</div>
                                             
											</div>


                                            <div class="cp-field" >
												<h5>City/Street</h5>
												<div class="cpp-fiel">
													<input type="text" name="city"  class=""  value="{{$data->city}}" placeholder="City/Street"  required>
													<i class="fa fa-map-marker"></i>
												</div>
                                             
											</div>

                                            <div class="cp-field" >
												<h5>Date Of Birth</h5>
												<div class="cpp-fiel">
													<input type="date" name="date_of_birth" value="{{$data->date_of_birth}}" class="" placeholder=""  required>
													<i class="fa fa-calendar"></i>
												</div>
                                             
											</div>
										
											<div class="save-stngs pd3">
												<ul>
                                                    <li><button type="submit" id="submit">Save</button></li>
												</ul>
											</div><!--save-stngs end-->
										</form>
									</div>
                                    </div>
                                    <div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  $('#kyc').hide();
$('.user_type').change(function(){
   let type = $(this).val();
   if(type==0){
       $('#aadhar').show('slow');
       $('#kyc').hide('slow');
       $('#kycField').attr('disabled' ,'disabled');
       $('#aadhar_card_number').attr('disabled',false);
       $('#submit').html('Enter aadhar Continue');
   }else{
    $('#aadhar').hide('slow');
       $('#kyc').show('slow');
       $('#aadhar_card_number').attr('disabled','disabled');
       $('#kycField').attr('disabled',false);
       $('#submit').html('Upload and Continue');
   }
})
</script>
@endsection
