<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/check-user', function () {
    return view('check_user');
});
Route::post('/update-user-info', 'HomeController@updateUserInfo')->name('update-user-info');
Route::group(['middleware' => 'CheckUser'], function()
{
Route::get('/', 'HomeController@complaint')->name('index');


Route::get('/add-anncouncement', function () {
    return view('add_anncounce');
});




Route::get('/home', 'HomeController@complaint')->name('home');
Route::get('/get-offcier', 'HomeController@getOffcier')->name('get-offcier');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('/edit-profile', 'HomeController@Editprofile')->name('Editprofile');
Route::post('/update-user', 'HomeController@updateUser')->name('updateUser');
Route::get('/anncouncement', 'HomeController@anncouncement')->name('announcement');

Route::get('/complaint', 'HomeController@complaint')->name('complaint');
Route::post('/addAnnouncement', 'HomeController@addAnnouncement')->name('addAnnouncement');


Route::get('/post-complaint', 'HomeController@postComplaint')->name('postComplaint');

Route::post('/post-complaint', 'HomeController@save_postComplaint')->name('postComplaint-save');


Route::post('/comments', 'HomeController@addComment')->name('comments');
// Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('/single_complaint', 'HomeController@single_complaint')->name('single_complaint');

});
